import sys

from re import split
from heapq import heappush, heappop

def find_shortest_paths(graph, start_v, search=None):
    explored = {}
    heap = []
    vertex = start_v
    weight = 0
    distances = {vertex: weight}
    while True:
        # push to heap
        try:
            for v, w in graph[vertex]:
                if not explored.has_key(v):
                    heappush(heap, (w + weight, v))
        except KeyError:
            break
        if not heap:
            break

        # pop from heap
        weight, vertex = heappop(heap)
        explored[vertex] = True
        if not distances.has_key(vertex):
            distances[vertex] = weight

    # add non-reachable vertices
    for v in graph.keys():
        if not distances.has_key(v):
            distances[v] = 1000000

    if not search:
        return distances.values()
    else:
        return [ distances[v] for v in search]

def load_graph(filename):
    gr = {}
    with open(filename) as f:
        for line in f.readlines():
            v = None
            for entry in split(r'\s+', line.strip()):
                if not v:
                    v = int(entry)
                    gr[v] = []
                else:
                    gr[v].append(tuple(map(int, entry.split(','))))

    return gr

if __name__ == '__main__':
    inputGraph = load_graph(sys.argv[1])
    distances = find_shortest_paths(inputGraph, 1, [7,37,59,82,99,115,133,165,188,197])
    # distances = find_shortest_paths(inputGraph, 13, [66,93,109,119,129])
    # distances = find_shortest_paths(inputGraph, 1, range(1,7))
    print(','.join(map(str, distances)))
