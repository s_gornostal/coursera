import sys

inversions = 0


def _merge(l1, l2):
    global inversions
    i = 0
    j = 0
    merged = []
    while len(l1) > i and len(l2) > j:
        if l1[i] <= l2[j]:
            merged.append(l1[i])
            i += 1
        else:
            merged.append(l2[j])
            j += 1
            inversions += len(l1) - i
    if len(l1) > i:
        merged.extend(l1[i:])
    if len(l2) > j:
        merged.extend(l2[j:])

    return merged


def merge_sort(l):
    if len(l) > 1:
        middle = len(l) / 2
        return _merge(merge_sort(l[:middle]), merge_sort(l[middle:]))
    else:
        return l


def load_array(filename):
    with open(filename) as f:
        return map(int, f)


if __name__ == '__main__':
    inputArray = load_array(sys.argv[1])
    s = merge_sort(inputArray)
    print inversions
