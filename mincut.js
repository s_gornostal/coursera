#!/usr/bin/env node
/*jshint node:true, latedef:false */
/*global require:true, process:true */

var filename = process.argv[2];

var fs = require('fs');
fs.readFile( filename, function (err, data) {
    if (err) {
        throw err;
    }

    var list = createadjAcencyList(data.toString()),
        mcs = [];
    for( var i = 0; i < list.length; i++ ){
        mcs.push( findMinCut(deepCopy(list)) );
    }
    // console.log(mcs);
    console.log('minimum cut - ', Math.min.apply(this, mcs));
});

function deepCopy(o) {
    var copy = o,k;
 
    if (o && typeof o === 'object') {
        copy = Object.prototype.toString.call(o) === '[object Array]' ? [] : {};
        for (k in o) {
            copy[k] = deepCopy(o[k]);
        }
    }
 
    return copy;
}

function findMinCut( list ){
    while( list.length > 2 ){
        var i = rand(list.length - 1),
            j = rand(list[i].nodes.length - 1);

        // console.log(i,j,'remove',list[i].nodes[j]);
        contractEdge(list, i, j);
    }

    // console.log('---------');
    // console.log(list);
    return list[0].nodes.length;
}

function isValid( list ){
    var opts = [];
    for(var i = 0; i < list.length; i++){
        opts.push(list[i].item);
    }
    for(i = 0; i < list.length; i++){
        for(var j = 0; j < list[i].nodes.length; j++){
            if(opts.indexOf(list[i].nodes[j]) < 0){
                return false;
            }
        }
    }
    return true;
}

function contractEdge( list, i, j ){
    try{
        var remove = list[i].nodes[j],
            rmIdx = findNode( list, remove );
        list[i].nodes.splice(j, 1);

        for( var ni = 0; ni < list[rmIdx].nodes.length; ni++ ){
            var node = list[rmIdx].nodes[ni];
            if( node !== list[i].item ){
                list[i].nodes.push(node);
                var nodeIdx = findNode(list, node);
                list[nodeIdx].nodes[list[nodeIdx].nodes.indexOf(remove)] = list[i].item;
            } 
        }

        // remove nodes from i
        for( ni = 0; ni < list[i].nodes.length; ni++ ){
            if( remove === list[i].nodes[ni] ){
                list[i].nodes.splice(ni, 1);
                ni--;
            }
        }

        list.splice(rmIdx, 1);
    } catch(e) {
        console.log(e);
        // console.log(i,j,list);
        debugger;
    }

    if(!isValid(list)){
        debugger;
    }
}

function findNode( list, node ){
    for( var i = 0; i < list.length; i++){
        if(list[i].item === node){
            return i;
        }
    }
}

function createadjAcencyList( data ){
    var ret = [];
    data.split("\n").forEach(function(row){
        var node = {item: null, nodes: []};
        row.split("\t").forEach(function(item){
            if( !item.trim() ){
                return;
            }
            if( !node.item ){
                node.item = item;
            } else {
                node.nodes.push(item);
            }
        });
        if(node.item){
            ret.push(node);
        }
    });
    return ret;
}

function rand( max, min ){
    min = min || 0;
    return Math.floor(Math.random() * (max - min + 1)) + min;
}