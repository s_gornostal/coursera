import sys

from collections import Counter
from array import array

def find_order(rev_graph):
    order = array('I')

    keys = rev_graph.keys()
    v = keys[0]
    key_start = 1

    q = array('I')
    q.append(v)
    explored = {}
    while q:
        v = q.pop()
        explored[v] = True
        try:
            v2 = rev_graph[v].pop()
            while explored.get(v2):
                v2 = rev_graph[v].pop()
            q.append(v)
            v = v2
            # unexplored vertex has been found
            q.append(v)
        except (IndexError, KeyError):
            # vertex v doesn't have any unexplored heads
            order.append(v)
            if not q:
                while key_start < len(keys):
                    if not explored.get(keys[key_start]):
                        q.append(keys[key_start])
                        break
                    else:
                        v = None
                    key_start += 1

    return order


def find_sccs(graph, rev_graph):
    order_q = find_order(rev_graph)
    components = {}

    v = order_q.pop()
    components[v] = 1
    v_source = v

    q = array('I')
    q.append(v)
    explored = {}
    while q:
        v = q.pop()
        explored[v] = True
        try:
            v2 = graph[v].pop()
            while explored.get(v2):
                v2 = graph[v].pop()
            q.append(v)
            v = v2
            q.append(v)
        except (IndexError, KeyError):
            if not q:
                while order_q:
                    v = order_q.pop()
                    if not explored.get(v):
                        q.append(v)
                        v_source = v
                        components[v_source] = 1
                        break
                    else:
                        v = None
            else:
                components[v_source] += 1

    return components

def get_top_5(sccs):
    c = Counter(sccs)
    top_5 = c.most_common(5)
    result = array('I')
    for i in range(5):
        try:
            result.append(top_5[i][1])
        except:
            result.append(0)
    return ','.join(map(str, result))

def load_graph(filename):
    gr = {}
    rgr = {}
    with open(filename) as f:
        for line in f.readlines():
            if line.strip():
                (tail, head) = map(int, line.split(' ')[:2])
                try:
                    gr[tail].append(head)
                except KeyError:
                    gr[tail] = [head]

                try:
                    rgr[head].append(tail)
                except:
                    rgr[head] = [tail]

    return (gr, rgr)

if __name__ == '__main__':
    (inputGraph, reverseGraph) = load_graph(sys.argv[1])
    sccs = find_sccs(inputGraph, reverseGraph)
    print(get_top_5(sccs))
