import sys

MIN = 2500
MAX = 4000


def q1(numbers):
    # print(list(numbers))
    res = {}
    keys = numbers.keys()
    for t in range(MIN, MAX + 1):
        for x in keys:
            y = t - x
            if y in numbers and x != y:
                res[t] = 1
                # print('%s+%s=%s' % (x, y, t))
                break
    return len(res)


def load_q1(filename):
    with open(filename) as f:
        return dict.fromkeys(filter(lambda i: i < MAX, map(int, f)))


if __name__ == '__main__':
    numbers = load_q1(sys.argv[1])
    print(q1(numbers))
