import sys
from random import randint


def bubble_sort(l):
    for i in range(len(l) - 1, 0, -1):
        for j in range(i):
            if l[j] > l[j + 1]:
                tmp = l[j + 1]
                l[j + 1] = l[j]
                l[j] = tmp
    return l


def load_array(filename):
    with open(filename) as f:
        return map(int, f)


if __name__ == '__main__':
    # inputArray = load_array(sys.argv[1])
    inputArray = map(lambda x: randint(0, 10000), range(100))
    s = bubble_sort(inputArray)
    print s
    print s == sorted(inputArray)
