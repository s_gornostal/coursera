<?php

function quicksort(&$l, $start = 0, $end = null, &$cmp = 0, $pivotAlg = 'first'){
    $end = $end !== null ? $end : count($l);
    
    if ($end - $start < 2) {
        // echo implode(', ', array_slice($l, $start, $end-$start)).PHP_EOL;
        return;
    }

    $cmp += $end - $start - 1;
    
    switch ($pivotAlg) {
        case 'first':
            $pivotId = $start;
            break;
        case 'last':
            $pivotId = $end - 1;
            break;
        case 'median':
            $pivotId = (int) ceil(($end - $start)/2) + $start - 1;
            break;
    }
    $pivot = $l[$pivotId];
    // echo implode(', ', array_slice($l, $start, $end))." - pivot $pivot\n";
    
    $m = $start;
    for ($i = $m; $i < $end; $i++) {
        if ($pivotId === $i) {
            continue;
        }
        
        if ($pivot > $l[$i]) {
            if ($m !== $i){
                $tmp = $l[$m]; $l[$m] = $l[$i]; $l[$i] = $tmp;
            }
            if ($m === $pivotId){
                $pivotId = $i;
            }
            $m++;
        }
    }

    $l[$pivotId] = $l[$m];
    $l[$m] = $pivot;
    // echo implode(', ', array_slice($l, $start, $end-1)).' - end '.PHP_EOL;
    
    quicksort($l, $start, $m, $cmp, $pivotAlg);
    quicksort($l, $m + 1, $end, $cmp, $pivotAlg);
}


function main($filename){
    $l = explode("\n", file_get_contents($filename));
    $l = array_map('intval', $l);

    $c = $l;
    sort($c);
    
    $cmp = 0;
    quicksort($l, 0, null, $cmp, 'median');
    echo $cmp, PHP_EOL;
    var_dump($l === $c);
}
main($argv[1]);