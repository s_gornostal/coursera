"""
Question 1
In this programming problem and the next you'll code up the greedy algorithms
from lecture for minimizing the weighted sum of completion times.. Download
the text file here (http://spark-public.s3.amazonaws.com/algo2/datasets/jobs.txt).
This file describes a set of jobs with positive and integral weights and lengths.
It has the format
[number_of_jobs]
[job_1_weight] [job_1_length]
[job_2_weight] [job_2_length]
...

For example, the third line of the file is "74 59", indicating that
the second job has weight 74 and length 59. You should NOT assume that
edge weights or lengths are distinct.

Your task in this problem is to run the greedy algorithm that schedules jobs
in decreasing order of the difference (weight - length). Recall from lecture
that this algorithm is not always optimal.

IMPORTANT: if two jobs have equal difference (weight - length),
you should schedule the job with higher weight first.
Beware: if you break ties in a different way, you are likely to get
the wrong answer. You should report the sum of weighted completion times of
the resulting schedule --- a positive integer --- in the box below.

ADVICE: If you get the wrong answer, try out some small test cases to debug
your algorithm (and post your test cases to the discussion forum)!
"""

import sys
from re import split
from itertools import dropwhile

from heapq import heappop, heapify as heapq_heapify


def solve_question(heap):
    def completion_times_iter(heap):
        compl_time = 0
        try:
            while True:
                (_, weight, length) = heappop(heap)
                compl_time += length
                yield (weight, compl_time)
        except IndexError:
            return

    return sum(w * c for w, c in completion_times_iter(heap))


def heapify(data, weight_fn):
    data = list(data)
    max_weigth = max(w for w, _ in data)
    p_data = list((weight_fn(weight, length, max_weigth), weight, length) for weight, length in data)
    heapq_heapify(p_data)
    return p_data


def substact(weight, length, max_weigth):
    max_weigth = float(max_weigth)
    return -1 * ((weight - length) + (weight / max_weigth) * 0.1)


def load_data(filename):
    with open(filename) as f:
        return dropwhile(
            lambda x: len(x) < 2, filter(bool, map(
                lambda line: tuple(map(int, split(r'\s+', line.strip()))), f))
        )

if __name__ == '__main__':
    data = load_data(sys.argv[1])
    print(solve_question(heapify(data, substact)))
