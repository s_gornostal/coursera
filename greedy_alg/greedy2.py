"""
Question 2

For this problem, use the same data set as in the previous problem.

Your task now is to run the greedy algorithm that schedules jobs (optimally)
in decreasing order of the ratio (weight/length). In this algorithm, it does
not matter how you break ties. You should report the sum of weighted completion
times of the resulting schedule --- a positive integer --- in the box below.
"""

import sys
from greedy1 import load_data, heapify, solve_question


def divide(weight, length, max_weigth):
    return (-1. * weight) / length


if __name__ == '__main__':
    data = load_data(sys.argv[1])
    print(solve_question(heapify(data, divide)))
